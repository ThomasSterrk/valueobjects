<?php

namespace Normeccarenet\Valueobjects\Base\Strings;

use Normeccarenet\Valueobjects\Base\BaseValueObjectInterface;

/**
 * Interface BaseStringInterface
 *
 * @package Normeccarenet\Valueobjects\Base\Strings
 */
interface BaseStringInterface extends BaseValueObjectInterface
{
    /**
     * string
     *
     * @return string
     */
    public function string();
}
