<?php

namespace Normeccarenet\Valueobjects\Base\Strings;

use Normeccarenet\Valueobjects\Base\BaseValueObjectInterface;

/**
 * Interface BaseNumberInterface
 *
 * @package Normeccarenet\Valueobjects\Base\Strings
 */
interface BaseNumberInterface extends BaseValueObjectInterface
{
    /**
     * number
     *
     * @return string
     */
    public function number();
}
