<?php

namespace Normeccarenet\Valueobjects\Base\Strings;

/**
 * Class BaseEmail
 *
 * @package Normeccarenet\Valueobjects\Base\Strings
 */
abstract class BaseEmail extends BaseString implements BaseEmailInterface
{
    const VALUE_OBJECT_TYPE = 'base e-mail address';

    /**
     * string
     *
     * @return string
     */
    public function email()
    {
        return $this->value();
    }

    /**
     * validationExpression
     *
     * @return bool
     */
    public function validationExpression(): bool
    {
        return filter_var($this->value(), FILTER_VALIDATE_EMAIL);
    }
}
