<?php

namespace Normeccarenet\Valueobjects\Base\Strings;

use Normeccarenet\Valueobjects\Base\BaseValueObjectInterface;

/**
 * Interface BaseEmailInterface
 *
 * @package Normeccarenet\Valueobjects\Base\Strings
 */
interface BaseEmailInterface extends BaseValueObjectInterface
{
    /**
     * email
     *
     * @return string
     */
    public function email();
}
