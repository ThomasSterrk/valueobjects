<?php

namespace Normeccarenet\Valueobjects\Base\Strings;

use Normeccarenet\Valueobjects\Base\BaseValueObject;

/**
 * Class BaseString
 *
 * @package Normeccarenet\Valueobjects\Base\Strings
 */
abstract class BaseString extends BaseValueObject implements BaseStringInterface
{
    const VALUE_OBJECT_TYPE = 'base string';

    /**
     * string
     *
     * @return string
     */
    public function string()
    {
        return $this->value();
    }

    /**
     * ValidationExpression
     *
     * @return bool
     */
    public function validationExpression(): bool
    {
        return is_string($this->value());
    }
}
