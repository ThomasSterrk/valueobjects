<?php

namespace Normeccarenet\Valueobjects\Base\Strings;

/**
 * Class BaseNumber
 *
 * @package Normeccarenet\Valueobjects\Base\Strings
 */
abstract class BaseNumber extends BaseString implements BaseNumberInterface
{
    const VALUE_OBJECT_TYPE = 'base number';

    /**
     * number
     *
     * @return string
     */
    public function number()
    {
        return $this->value();
    }

    /**
     * validationExpression
     *
     * @return bool
     */
    public function validationExpression(): bool
    {
        return filter_var($this->value(), FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => "/^[a-zA-Z0-9]+$/")));
    }
}
