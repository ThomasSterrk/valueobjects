<?php

namespace Normeccarenet\Valueobjects\Base;

use Carbon\Exceptions\InvalidDateException;
use InvalidArgumentException;

/**
 * Class BaseValueObject
 *
 * @package Normeccarenet\Valueobjects\Base
 */
abstract class BaseValueObject implements BaseValueObjectInterface
{
    const VALUE_OBJECT_TYPE = 'base value object';

    /** @var mixed $value */
    protected $value;

    /**
     * BaseId constructor.
     *
     * @param string $value
     */
    public function __construct($value = null)
    {
        $this->initValue($value);
        $this->validate();
    }

    /**
     * __toString
     *
     * @return string
     */
    public function __toString(): string
    {
        return sprintf('%s', $this->value);
    }

    /**
     * value
     *
     * @return mixed
     */
    protected function value()
    {
        return $this->value;
    }

    /**
     * validate
     * @return void
     * @throws InvalidArgumentException
     */
    protected function validate()
    {
        if (!$this->validationExpression()) {
            $this->invalidArgumentException();
        }
    }

    /**
     * validationExpression
     *
     * @return bool
     */
    abstract function validationExpression(): bool;

    /**
     * initValue
     *
     * @param $value
     * @return void
     */
    protected function initValue($value): void
    {
        $this->value = null === $value ? '' : (string)$value;
    }

    /**
     * invalidArgumentException
     *
     * @return void
     * @throws InvalidArgumentException
     */
    protected function invalidArgumentException():void
    {
        throw new InvalidArgumentException('Value is not suitable as ' . static::VALUE_OBJECT_TYPE.'.');
    }
}
