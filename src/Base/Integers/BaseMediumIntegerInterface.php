<?php

namespace Normeccarenet\Valueobjects\Base\Integers;

use Normeccarenet\Valueobjects\Base\BaseValueObjectInterface;

/**
 * Interface BaseMediumIntegerInterface
 *
 * @package Normeccarenet\Valueobjects\Base\Integers
 */
interface BaseMediumIntegerInterface extends BaseValueObjectInterface
{
    /**
     * mediumInteger
     *
     * @return mixed
     */
    public function mediumInteger(): int;
}
