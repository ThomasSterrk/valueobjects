<?php

namespace Normeccarenet\Valueobjects\Base\Integers;

/**
 * Class BaseSmallInteger
 *
 * @package Normeccarenet\Valueobjects\Base\Integers
 */
abstract class BaseSmallInteger extends BaseInteger implements BaseSmallIntegerInterface
{
    const VALUE_OBJECT_TYPE = 'base small integer';

    const UNSIGNED = false;

    const MINIMUM_VALUE_SIGNED = -32768;
    const MAXIMUM_VALUE_SIGNED = 32767;

    const MINIMUM_VALUE_UNSIGNED = 0;
    const MAXIMUM_VALUE_UNSIGNED = 65535;

    /**
     * smallInteger
     *
     * @return string
     */
    public function smallInteger(): int
    {
        return $this->value();
    }
}
