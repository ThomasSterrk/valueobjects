<?php

namespace Normeccarenet\Valueobjects\Base\Integers;

/**
 * Class BaseBigInteger
 *
 * @package Normeccarenet\Valueobjects\Base\Integers
 */
abstract class BaseBigInteger extends BaseInteger implements BaseBigIntegerInterface
{
    const VALUE_OBJECT_TYPE = 'base big integer';

    const UNSIGNED = false;

    const MINIMUM_VALUE_SIGNED = -9223372036854775807;
    const MAXIMUM_VALUE_SIGNED = 9223372036854775807;

    const MINIMUM_VALUE_UNSIGNED = 0;
    const MAXIMUM_VALUE_UNSIGNED = 9223372036854775807;

    /**
     * bigInteger
     *
     * @return string
     */
    public function bigInteger(): int
    {
        return $this->value();
    }

    /**
     * initValue
     *
     * @param $value
     */
    protected function initValue($value): void
    {
        /**
         * Unsigned BigInteger is currently not supported
         */
        if (static::UNSIGNED) {
            $this->invalidArgumentException();
        }

        parent::initValue($value);
    }


}
