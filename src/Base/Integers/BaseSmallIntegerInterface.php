<?php

namespace Normeccarenet\Valueobjects\Base\Integers;

use Normeccarenet\Valueobjects\Base\BaseValueObjectInterface;

/**
 * Interface BaseSmallIntegerInterface
 *
 * @package Normeccarenet\Valueobjects\Base\Integers
 */
interface BaseSmallIntegerInterface extends BaseValueObjectInterface
{
    /**
     * smallInteger
     *
     * @return mixed
     */
    public function smallInteger(): int;
}
