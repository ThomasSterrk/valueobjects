<?php

namespace Normeccarenet\Valueobjects\Base\Integers;

/**
 * Class BaseId
 *
 * @package Normeccarenet\Valueobjects\Base\Integers
 */
abstract class BaseId extends BaseInteger implements BaseIdInterface
{
    const VALUE_OBJECT_TYPE = 'base ID';

    /**
     * id
     *
     * @return int
     */
    public function id()
    {
        return $this->value();
    }
}
