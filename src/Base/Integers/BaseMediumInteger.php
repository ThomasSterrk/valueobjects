<?php

namespace Normeccarenet\Valueobjects\Base\Integers;

/**
 * Class BaseMediumInteger
 *
 * @package Normeccarenet\Valueobjects\Base\Integers
 */
abstract class BaseMediumInteger extends BaseInteger implements BaseMediumIntegerInterface
{
    const VALUE_OBJECT_TYPE = 'base medium integer';

    const UNSIGNED = false;

    const MINIMUM_VALUE_SIGNED = -8388608;
    const MAXIMUM_VALUE_SIGNED = 8388607;

    const MINIMUM_VALUE_UNSIGNED = 0;
    const MAXIMUM_VALUE_UNSIGNED = 16777215;

    /**
     * mediumInteger
     *
     * @return mixed|string
     */
    public function mediumInteger(): int
    {
        return $this->value();
    }
}
