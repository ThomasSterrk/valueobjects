<?php

namespace Normeccarenet\Valueobjects\Base\Integers;

use Normeccarenet\Valueobjects\Base\BaseValueObjectInterface;

/**
 * Interface BaseIntegerInterface
 *
 * @package Normeccarenet\Valueobjects\Base\Integers
 */
interface BaseIntegerInterface extends BaseValueObjectInterface
{
    /**
     * value
     *
     * @return int
     */
    public function integer(): int;
}
