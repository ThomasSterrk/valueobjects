<?php

namespace Normeccarenet\Valueobjects\Base\Integers;

use Normeccarenet\Valueobjects\Base\BaseValueObjectInterface;

/**
 * Interface BaseBigIntegerInterface
 *
 * @package Normeccarenet\Valueobjects\Base\Integers
 */
interface BaseBigIntegerInterface extends BaseValueObjectInterface
{
    /**
     * bigInteger
     *
     * @return mixed
     */
    public function bigInteger(): int;
}
