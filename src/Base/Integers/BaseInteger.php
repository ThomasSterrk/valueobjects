<?php

namespace Normeccarenet\Valueobjects\Base\Integers;

use Normeccarenet\Valueobjects\Base\BaseValueObject;

/**
 * Class BaseInteger
 *
 * @package Normeccarenet\Valueobjects\Base\Integers
 */
abstract class BaseInteger extends BaseValueObject implements BaseIntegerInterface
{
    const VALUE_OBJECT_TYPE = 'base integer';

    const UNSIGNED = false;

    const MINIMUM_VALUE_SIGNED = -2147483648;
    const MAXIMUM_VALUE_SIGNED = 2147483647;

    const MINIMUM_VALUE_UNSIGNED = 0;
    const MAXIMUM_VALUE_UNSIGNED = 4294967295;

    /**
     * integer
     *
     * @return string
     */
    public function integer(): int
    {
        return $this->value();
    }

    /**
     * initValue
     *
     * @param $value
     */
    protected function initValue($value): void
    {
        $this->value = null === $value ? 0 : $value;
    }

    /**
     * validationExpression
     *
     * @return bool
     */
    public function validationExpression(): bool
    {
        if (!is_integer($this->value())) {
            $this->invalidArgumentException();
        }

        return static::UNSIGNED
            ? $this->value >= static::MINIMUM_VALUE_UNSIGNED && $this->value <= static::MAXIMUM_VALUE_UNSIGNED
            : $this->value >= static::MINIMUM_VALUE_SIGNED && $this->value <= static::MAXIMUM_VALUE_SIGNED;
    }
}
