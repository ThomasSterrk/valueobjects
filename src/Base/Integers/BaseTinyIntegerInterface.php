<?php

namespace Normeccarenet\Valueobjects\Base\Integers;

use Normeccarenet\Valueobjects\Base\BaseValueObjectInterface;

/**
 * Interface BaseTinyIntegerInterface
 *
 * @package Normeccarenet\Valueobjects\Base\Integers
 */
interface BaseTinyIntegerInterface extends BaseValueObjectInterface
{
    /**
     * tinyInteger
     *
     * @return mixed
     */
    public function tinyInteger(): int;
}
