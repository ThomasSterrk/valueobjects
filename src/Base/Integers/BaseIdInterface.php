<?php

namespace Normeccarenet\Valueobjects\Base\Integers;

use Normeccarenet\Valueobjects\Base\BaseValueObjectInterface;

/**
 * Interface BaseIdInterface
 *
 * @package Normeccarenet\Valueobjects\Base\Integers
 */
interface BaseIdInterface extends BaseValueObjectInterface
{
    /**
     * id
     *
     * @return int
     */
    public function id();
}
