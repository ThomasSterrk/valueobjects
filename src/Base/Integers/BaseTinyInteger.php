<?php

namespace Normeccarenet\Valueobjects\Base\Integers;

/**
 * Class BaseTinyInteger
 *
 * @package Normeccarenet\Valueobjects\Base\Integers
 */
abstract class BaseTinyInteger extends BaseInteger implements BaseTinyIntegerInterface
{
    const VALUE_OBJECT_TYPE = 'base tiny integer';

    const UNSIGNED = false;

    const MINIMUM_VALUE_SIGNED = -128;
    const MAXIMUM_VALUE_SIGNED = 127;

    const MINIMUM_VALUE_UNSIGNED = 0;
    const MAXIMUM_VALUE_UNSIGNED = 255;

    /**
     * tinyInteger
     *
     * @return string
     */
    public function tinyInteger(): int
    {
        return $this->value();
    }
}
