<?php

namespace Normeccarenet\Valueobjects\Base;

/**
 * Interface BaseValueObjectInterface
 *
 * @package Normeccarenet\Valueobjects\Base
 */
interface BaseValueObjectInterface
{
    /**
     * __toString
     *
     * @return string
     */
    public function __toString(): string;
}
