<?php

namespace Normeccarenet\Valueobjects\Tests;

use PHPUnit\Framework\TestCase;

use Normeccarenet\Valueobjects\Base\Strings\BaseString;

class BaseStringTest extends TestCase
{
    /** @test */
    public function returnsValue()
    {
        $stub = new Class('this is a test string') extends BaseString
        {
            public function validationExpression(): bool
            {
                return true;
            }
        };

        $this->assertEquals('this is a test string', $stub->string());
    }
}
