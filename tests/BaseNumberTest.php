<?php

namespace Normeccarenet\Valueobjects\Tests;

use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

use Normeccarenet\Valueobjects\Base\Strings\BaseNumber;

class BaseNumberTest extends TestCase
{
    /** @test */
    public function shouldValidateCorrectly()
    {
        $stub = new Class('kjhg42k897f3b347chalpci') extends BaseNumber {};
        $this->assertEquals('kjhg42k897f3b347chalpci', $stub->string());
    }

    /** @test */
    public function missingNumber()
    {
        $this->expectException(InvalidArgumentException::class);
        new Class() extends BaseNumber {};
    }

    /** @test */
    public function noEmojis()
    {
        $this->expectException(InvalidArgumentException::class);
        new Class('🤓') extends BaseNumber {};
    }

}
