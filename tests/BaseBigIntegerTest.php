<?php

namespace Normeccarenet\Valueobjects\Tests;

use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

use Normeccarenet\Valueobjects\Base\Integers\BaseBigInteger;

class BaseBigIntegerTest extends TestCase
{
    /** @test */
    public function shouldValidateCorrectly()
    {
        /**
         * Signed
         */
        $stub = new Class(-9223372036854775807) extends BaseBigInteger {};
        $this->assertEquals(-9223372036854775807, $stub->bigInteger());

        $stub = new Class(9223372036854775807) extends BaseBigInteger {};
        $this->assertEquals(9223372036854775807, $stub->bigInteger());
    }

    /** @test */
    public function tooSmallSigned()
    {
        $this->expectException(InvalidArgumentException::class);
        new Class(-9223372036854775808) extends BaseBigInteger {};
    }

    /** @test */
    public function tooBigSigned()
    {
        $this->expectException(InvalidArgumentException::class);
        new Class(9223372036854775808) extends BaseBigInteger {};
    }

    /** @test */
    public function unsignedThrowsException()
    {
        $this->expectException(InvalidArgumentException::class);
        new Class() extends BaseBigInteger { const UNSIGNED = true; };
    }
}
