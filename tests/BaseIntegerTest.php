<?php

namespace Normeccarenet\Valueobjects\Tests;

use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

use Normeccarenet\Valueobjects\Base\Integers\BaseInteger;

class BaseIntegerTest extends TestCase
{
    /** @test */
    public function shouldValidateCorrectly()
    {
        /**
         * Signed
         */
        $stub = new Class(-2147483648) extends BaseInteger {};
        $this->assertEquals(-2147483648, $stub->integer());

        $stub = new Class(2147483647) extends BaseInteger {};
        $this->assertEquals(2147483647, $stub->integer());

        /**
         * Unsigned
         */
        $stub = new Class(0) extends BaseInteger { const UNSIGNED = true; };
        $this->assertEquals(0, $stub->integer());

        $stub = new Class(4294967295) extends BaseInteger { const UNSIGNED = true; };
        $this->assertEquals(4294967295, $stub->integer());
    }

    /** @test */
    public function tooSmallSigned()
    {
        $this->expectException(InvalidArgumentException::class);
        new Class(-2147483649) extends BaseInteger {};
    }

    /** @test */
    public function tooBigSigned()
    {
        $this->expectException(InvalidArgumentException::class);
        new Class(2147483648) extends BaseInteger {};
    }

    /** @test */
    public function tooSmallUnSigned()
    {
        $this->expectException(InvalidArgumentException::class);
        new Class(-1) extends BaseInteger { const UNSIGNED = true; };
    }

    /** @test */
    public function tooBigUnSigned()
    {
        $this->expectException(InvalidArgumentException::class);
        new Class(4294967296) extends BaseInteger { const UNSIGNED = true; };
    }
}
