<?php

namespace Normeccarenet\Valueobjects\Tests;

use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

use Normeccarenet\Valueobjects\Base\BaseValueObject;

class AbstractBaseValueObjectTest extends TestCase
{
    /** @test */
    public function argumentInvalid()
    {
        $this->expectException(InvalidArgumentException::class);

        new Class() extends BaseValueObject {
            public function validationExpression(): bool
            {
                return false;
            }
        };
    }

    /** @test */
    public function alwaysReturnsAString()
    {
        //integer
        $stub = new Class(12345) extends BaseValueObject {
            public function validationExpression(): bool
            {
                return true;
            }
        };

        $this->assertIsString($stub->__toString());


        // boolean
        $stub = new Class(false) extends BaseValueObject {
            public function validationExpression(): bool
            {
                return true;
            }
        };

        $this->assertIsString($stub->__toString());


        // float
        $stub = new Class(12345.6789) extends BaseValueObject {
            public function validationExpression(): bool
            {
                return true;
            }
        };

        $this->assertIsString($stub->__toString());

        // @TODO - array
    }

    /** @test */
    public function validationAlwaysReturnBoolean()
    {
        $stub = new Class('this is a test string') extends BaseValueObject {
            public function validationExpression(): bool
            {
                return true;
            }
        };

        $this->assertIsBool($stub->validationExpression());

        $stub = new Class('this is a test string') extends BaseValueObject {
            public function validationExpression(): bool
            {
                return 'false';
            }
        };

        $this->assertIsBool($stub->validationExpression());

        $stub = new Class('this is a test string') extends BaseValueObject {
            public function validationExpression(): bool
            {
                return 1;
            }
        };

        $this->assertIsBool($stub->validationExpression());
    }
}
