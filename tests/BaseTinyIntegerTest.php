<?php

namespace Normeccarenet\Valueobjects\Tests;

use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

use Normeccarenet\Valueobjects\Base\Integers\BaseTinyInteger;

class BaseTinyIntegerTest extends TestCase
{
    /** @test */
    public function shouldValidateCorrectly()
    {
        /**
         * Signed
         */
        $stub = new Class(-128) extends BaseTinyInteger {};
        $this->assertEquals(-128, $stub->tinyInteger());

        $stub = new Class(127) extends BaseTinyInteger {};
        $this->assertEquals(127, $stub->tinyInteger());

        /**
         * Unsigned
         */
        $stub = new Class(0) extends BaseTinyInteger { const UNSIGNED = true; };
        $this->assertEquals(0, $stub->tinyInteger());

        $stub = new Class(255) extends BaseTinyInteger { const UNSIGNED = true; };
        $this->assertEquals(255, $stub->tinyInteger());
    }

    /** @test */
    public function tooSmallSigned()
    {
        $this->expectException(InvalidArgumentException::class);
        new Class(-129) extends BaseTinyInteger {};
    }

    /** @test */
    public function tooBigSigned()
    {
        $this->expectException(InvalidArgumentException::class);
        new Class(128) extends BaseTinyInteger {};
    }

    /** @test */
    public function tooSmallUnSigned()
    {
        $this->expectException(InvalidArgumentException::class);
        new Class(-1) extends BaseTinyInteger { const UNSIGNED = true; };
    }

    /** @test */
    public function tooBigUnSigned()
    {
        $this->expectException(InvalidArgumentException::class);
        new Class(256) extends BaseTinyInteger { const UNSIGNED = true; };
    }
}
