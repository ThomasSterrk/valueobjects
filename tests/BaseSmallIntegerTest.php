<?php

namespace Normeccarenet\Valueobjects\Tests;

use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

use Normeccarenet\Valueobjects\Base\Integers\BaseSmallInteger;

class BaseSmallIntegerTest extends TestCase
{
    /** @test */
    public function shouldValidateCorrectly()
    {
        /**
         * Signed
         */
        $stub = new Class(-32768) extends BaseSmallInteger {};
        $this->assertEquals(-32768, $stub->smallInteger());

        $stub = new Class(32767) extends BaseSmallInteger {};
        $this->assertEquals(32767, $stub->smallInteger());

        /**
         * Unsigned
         */
        $stub = new Class(0) extends BaseSmallInteger { const UNSIGNED = true; };
        $this->assertEquals(0, $stub->smallInteger());

        $stub = new Class(65535) extends BaseSmallInteger { const UNSIGNED = true; };
        $this->assertEquals(65535, $stub->smallInteger());
    }

    /** @test */
    public function tooSmallSigned()
    {
        $this->expectException(InvalidArgumentException::class);
        new Class(-32769) extends BaseSmallInteger {};
    }

    /** @test */
    public function tooBigSigned()
    {
        $this->expectException(InvalidArgumentException::class);
        new Class(32768) extends BaseSmallInteger {};
    }

    /** @test */
    public function tooSmallUnSigned()
    {
        $this->expectException(InvalidArgumentException::class);
        new Class(-1) extends BaseSmallInteger { const UNSIGNED = true; };
    }

    /** @test */
    public function tooBigUnSigned()
    {
        $this->expectException(InvalidArgumentException::class);
        new Class(65536) extends BaseSmallInteger { const UNSIGNED = true; };
    }
}
