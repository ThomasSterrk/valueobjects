<?php

namespace Normeccarenet\Valueobjects\Tests;

use PHPUnit\Framework\TestCase;

use Normeccarenet\Valueobjects\Base\Integers\BaseId;

class BaseIDTest extends TestCase
{
    /** @test */
    public function alwaysReturnsInteger()
    {
        $stub = new Class(123456789) extends BaseId {};
        $this->assertEquals(123456789, $stub->id());
    }
}
