<?php

namespace Normeccarenet\Valueobjects\Tests;

use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

use Normeccarenet\Valueobjects\Base\Strings\BaseEmail;

class BaseEmailTest extends TestCase
{
    /** @test */
    public function missingDomain()
    {
        $this->expectException(InvalidArgumentException::class);
        new Class('kevin@') extends BaseEmail {};
    }

    /** @test */
    public function invalidDomain()
    {
        $this->expectException(InvalidArgumentException::class);
        new Class('kevin@sterrk') extends BaseEmail {};
    }

    /** @test */
    public function missingMonkeySign()
    {
        $this->expectException(InvalidArgumentException::class);
        new Class('kevinsterrk.nl') extends BaseEmail {};
    }

    /** @test */
    public function missingPrefix()
    {
        $this->expectException(InvalidArgumentException::class);
        new Class('@sterrk.nl') extends BaseEmail {};
    }

    /** @test */
    public function invalidPrefix()
    {
        $this->expectException(InvalidArgumentException::class);
        new Class('``^$@&_@sterrk.nl') extends BaseEmail {};
    }

    /** @test */
    public function stringOnly()
    {
        $this->expectException(InvalidArgumentException::class);
        new Class(8397546) extends BaseEmail {};
    }

    /** @test */
    public function shouldValidateCorrectly()
    {
        $stub = new Class('kevin@sterrk.nl') extends BaseEmail {};
        $this->assertEquals('kevin@sterrk.nl', $stub->email());

        $stub = new Class('kevin+@sterrk.nl') extends BaseEmail {};
        $this->assertEquals('kevin+@sterrk.nl', $stub->email());

        $stub = new Class('%$^&%^@sterrk.nl') extends BaseEmail {};
        $this->assertEquals('%$^&%^@sterrk.nl', $stub->email());
    }
}
