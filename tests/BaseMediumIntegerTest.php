<?php

namespace Normeccarenet\Valueobjects\Tests;

use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

use Normeccarenet\Valueobjects\Base\Integers\BaseMediumInteger;

class BaseMediumIntegerTest extends TestCase
{
    /** @test */
    public function shouldValidateCorrectly()
    {
        /**
         * Signed
         */
        $stub = new Class(-8388608) extends BaseMediumInteger {};
        $this->assertEquals(-8388608, $stub->mediumInteger());

        $stub = new Class(8388607) extends BaseMediumInteger {};
        $this->assertEquals(8388607, $stub->mediumInteger());

        /**
         * Unsigned
         */
        $stub = new Class(0) extends BaseMediumInteger { const UNSIGNED = true; };
        $this->assertEquals(0, $stub->mediumInteger());

        $stub = new Class(16777215) extends BaseMediumInteger { const UNSIGNED = true; };
        $this->assertEquals(16777215, $stub->mediumInteger());
    }

    /** @test */
    public function tooSmallSigned()
    {
        $this->expectException(InvalidArgumentException::class);
        new Class(-8388609) extends BaseMediumInteger {};
    }

    /** @test */
    public function tooBigSigned()
    {
        $this->expectException(InvalidArgumentException::class);
        new Class(8388608) extends BaseMediumInteger {};
    }

    /** @test */
    public function tooSmallUnSigned()
    {
        $this->expectException(InvalidArgumentException::class);
        new Class(-1) extends BaseMediumInteger { const UNSIGNED = true; };
    }

    /** @test */
    public function tooBigUnSigned()
    {
        $this->expectException(InvalidArgumentException::class);
        new Class(16777216) extends BaseMediumInteger { const UNSIGNED = true; };
    }

}
